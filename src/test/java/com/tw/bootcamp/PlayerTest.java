package com.tw.bootcamp;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class PlayerTest {

    @Test
    public void shouldReturnCheatForAlwaysCheatPlayer() {

        Player player = new Player(new AlwaysCheat());

        assertEquals(Move.CHEAT,player.getMove());
        assertEquals(Move.CHEAT,player.getMove());
    }

    @Test
    public void shouldReturnCooperateForAlwaysCooperatePlayer() {

        Player player = new Player(new AlwaysCooperate());

        assertEquals(Move.COOPERATE,player.getMove());
        assertEquals(Move.COOPERATE,player.getMove());
    }

    @Test
    public void shouldReturnAlternateStratgeyForAlternatePlayer() {

        Player player = new Player(new AlternateStrategy());

        assertEquals(Move.COOPERATE,player.getMove());
        assertEquals(Move.CHEAT,player.getMove());
        assertEquals(Move.COOPERATE,player.getMove());
    }

    @Test
    public void shouldAskForNextMoveForConsoleStrategyPlayer() {

        ConsoleStrategy mockConsoleStrategy = Mockito.mock(ConsoleStrategy.class);
        Player player = new Player(mockConsoleStrategy);

        //mock input for console strategy and reply with CHEAT and COOPERATE in this order
        when(mockConsoleStrategy.getNextMove()).thenReturn(Move.CHEAT).thenReturn(Move.COOPERATE);

        assertEquals(Move.CHEAT,player.getMove());
        assertEquals(Move.COOPERATE,player.getMove());
    }
/*
    @Test
    public void shouldStartWithCooperateMoveForCopyCatPlayer() {
        Player player = new Player(new CopyCatStrategy());

        assertEquals(Move.COOPERATE, player.getMove());

    }*/

    @Test
    public void shouldReturnLastMoveAndCurrentMove() {
        Player player = new Player(new AlternateStrategy());
        player.getMove();
        player.getMove();
        assertEquals(Move.CHEAT, player.getLastMove());
    }

}