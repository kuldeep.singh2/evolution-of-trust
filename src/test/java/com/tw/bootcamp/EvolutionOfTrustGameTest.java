package com.tw.bootcamp;

import org.junit.jupiter.api.Test;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EvolutionOfTrustGameTest {

    @Test
    public void shouldReturnGameResultAfterTwoRoundsWhenPlayerOneAlwaysCheatPlayerTwoAlwaysCooperate() {
        int numberOfRounds = 2;

        Player player1 = new Player(new AlwaysCheat());
        Player player2 = new Player(new AlwaysCooperate());
        RuleEngine ruleEngine = new RuleEngine();
        String score = new EvolutionOfTrustGame(ruleEngine, numberOfRounds).play(player1, player2);
        assertEquals("Player 1 coins is 6" + " Player 2 coins is -2", score);
    }

    @Test
    public void shouldReturnGameResultAfterTwoRoundsWhenPlayerOneAlwaysCooperatePlayerTwoAlwaysCheat() {
        int numberOfRounds = 2;

        Player player1 = new Player(new AlwaysCooperate());
        Player player2 = new Player(new AlwaysCheat());
        RuleEngine ruleEngine = new RuleEngine();
        String score = new EvolutionOfTrustGame(ruleEngine, numberOfRounds).play(player1, player2);
        assertEquals("Player 1 coins is -2" + " Player 2 coins is 6", score);
    }

    @Test
    public void shouldReturnGameResultAfterTwoRoundsWhenPlayerOneAlwaysCheatAndPlayerTeoAlternate() {
        int numberOfRounds = 2;

        Player player1 = new Player(new AlwaysCheat());
        Player player2 = new Player(new AlternateStrategy());
        RuleEngine ruleEngine = new RuleEngine();
        String score = new EvolutionOfTrustGame(ruleEngine, numberOfRounds).play(player1, player2);
        assertEquals("Player 1 coins is 3" + " Player 2 coins is -1", score);
    }

    @Test
    public void shouldReturnGameResultAfterTwoRoundsWhenPlayerOneAlternateAndPlayerTwoAlwaysCheat() {
        int numberOfRounds = 2;

        Player player1 = new Player(new AlternateStrategy());
        Player player2 = new Player(new AlwaysCheat());
        RuleEngine ruleEngine = new RuleEngine();
        String score = new EvolutionOfTrustGame(ruleEngine, numberOfRounds).play(player1, player2);
        assertEquals("Player 1 coins is -1" + " Player 2 coins is 3", score);
    }

    @Test
    public void shouldReturnGameResultAfterTwoRoundsWhenPlayerOneAlternateAndPlayerTwoAlwaysCooperate() {
        int numberOfRounds = 2;

        Player player1 = new Player(new AlternateStrategy());
        Player player2 = new Player(new AlwaysCooperate());
        RuleEngine ruleEngine = new RuleEngine();
        String score = new EvolutionOfTrustGame(ruleEngine, numberOfRounds).play(player1, player2);
        assertEquals("Player 1 coins is 5" + " Player 2 coins is 1", score);
    }

    @Test
    public void shouldReturnGameResultAfterTwoRoundsWhenPlayerOneAlwaysCooperateAndPlayerTwoAlternate() {
        int numberOfRounds = 2;

        Player player1 = new Player(new AlwaysCooperate());
        Player player2 = new Player(new AlternateStrategy());
        RuleEngine ruleEngine = new RuleEngine();
        String score = new EvolutionOfTrustGame(ruleEngine, numberOfRounds).play(player1, player2);
        assertEquals("Player 1 coins is 1" + " Player 2 coins is 5", score);
    }

    @Test
    public void shouldReturnGameResultAfterTwoRoundsWhenPlayerOneAlwaysCooperateAndPlayerTwoPlaysAsConsolePlayer() {
        int numberOfRounds = 2;

        Scanner scanner = new Scanner("CHEAT\nCOOPERATE\n");
        Player player1 = new Player(new AlwaysCooperate());
        Player player2 = new Player(new ConsoleStrategy(scanner));
        RuleEngine ruleEngine = new RuleEngine();

        String score = new EvolutionOfTrustGame(ruleEngine, numberOfRounds).play(player1, player2);

        assertEquals("Player 1 coins is 1" + " Player 2 coins is 5", score);
    }


    /*public static void main(String[] args) {
        int numberOfRounds = 2;

        Player player1 = new Player(new AlwaysCooperate());
        Player player2 = new Player(new ConsoleStrategy(new Scanner(System.in)));
        RuleEngine ruleEngine = new RuleEngine();

        EvolutionOfTrustGame game = new EvolutionOfTrustGame(ruleEngine, numberOfRounds);
        String score = game.play(player1, player2);
        System.out.println(score);
    }*/
}
