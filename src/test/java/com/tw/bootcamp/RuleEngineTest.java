package com.tw.bootcamp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RuleEngineTest {

    @Test
    public void shouldReturnGameResultWhenBothPlayersCooperate() {
        RuleEngine ruleEngine = new RuleEngine();
        ruleEngine.play(Move.COOPERATE, Move.COOPERATE);

        assertEquals(2, ruleEngine.getPlayerOneCoins());
        assertEquals(2, ruleEngine.getPlayerTwoCoins());
    }

    @Test
    public void shouldReturnGameResultWhenPlayerOneCheatAndPlayerTwoCooperate() {
        RuleEngine ruleEngine = new RuleEngine();
        ruleEngine.play(Move.CHEAT, Move.COOPERATE);

        assertEquals(3, ruleEngine.getPlayerOneCoins());
        assertEquals(-1, ruleEngine.getPlayerTwoCoins());
    }

    @Test
    public void shouldReturnGameResultWhenBothPlayersCheat() {
        RuleEngine ruleEngine = new RuleEngine();
        ruleEngine.play(Move.CHEAT, Move.CHEAT);

        assertEquals(0, ruleEngine.getPlayerOneCoins());
        assertEquals(0, ruleEngine.getPlayerTwoCoins());
    }

    @Test
    public void shouldReturnGameResultWhenPlayerOneCooperateAndPlayerTwoCheat() {
        RuleEngine ruleEngine = new RuleEngine();
        ruleEngine.play(Move.COOPERATE, Move.CHEAT);

        assertEquals(-1, ruleEngine.getPlayerOneCoins());
        assertEquals(3, ruleEngine.getPlayerTwoCoins());
    }
}


