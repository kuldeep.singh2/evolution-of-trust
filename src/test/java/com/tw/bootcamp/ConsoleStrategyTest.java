package com.tw.bootcamp;

import org.junit.jupiter.api.Test;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ConsoleStrategyTest {

    @Test
    public void shouldReturnMoveForConsoleStrategy() {
        Scanner scanner = new Scanner("CHEAT\nCOOPERATE\n");
        Strategy consoleStrategy = new ConsoleStrategy(scanner);

        assertEquals(Move.CHEAT, consoleStrategy.getNextMove());
        assertEquals(Move.COOPERATE, consoleStrategy.getNextMove());
    }
}