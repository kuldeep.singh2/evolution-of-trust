package com.tw.bootcamp;

import java.util.Scanner;

public class ConsoleStrategy extends Strategy {
    private final Scanner scanner;

    public ConsoleStrategy(Scanner scanner) {
        this.scanner = scanner;
    }

    @Override
    public Move getNextMove() {
        System.out.println("Input your next move: ");
        return Move.valueOf(scanner.nextLine());
    }
}
