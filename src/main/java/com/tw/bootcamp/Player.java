package com.tw.bootcamp;

public class Player {
    private Strategy strategy;
    private Move lastMove;

    public Player(Strategy strategy) {
        this.strategy = strategy;
    }

    public Move getMove() {
        Move currentMove = this.strategy.getNextMove();
        lastMove = currentMove;
        return currentMove;
    }

    public Move getLastMove() {
        return lastMove;
    }
}
