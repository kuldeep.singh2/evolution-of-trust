package com.tw.bootcamp;

public class AlternateStrategy extends Strategy {
    private Move lastMove = Move.CHEAT;

    @Override
    public Move getNextMove() {
        Move currentMove = (lastMove == Move.CHEAT) ? Move.COOPERATE : Move.CHEAT;
        lastMove = currentMove;
        return currentMove;
    }
}
