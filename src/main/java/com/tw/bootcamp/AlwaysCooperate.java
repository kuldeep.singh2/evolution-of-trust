package com.tw.bootcamp;

public class AlwaysCooperate extends Strategy {
    @Override
    public Move getNextMove() {
        return Move.COOPERATE;
    }
}
