package com.tw.bootcamp;

public class RuleEngine {

    private int playerOneCoins;
    private int playerTwoCoins;
    
    public void play(Move playerOneMove, Move playerTwoMove) {
        if (playerTwoMove == Move.COOPERATE && playerOneMove == Move.COOPERATE) {
            playerOneCoins = playerOneCoins + 2;
            playerTwoCoins = playerTwoCoins + 2;
        }
        if (playerTwoMove == Move.COOPERATE && playerOneMove == Move.CHEAT) {
            playerOneCoins = playerOneCoins + 3;
            playerTwoCoins = playerTwoCoins - 1;
        }
        if (playerTwoMove == Move.CHEAT && playerOneMove == Move.COOPERATE) {
            playerOneCoins = playerOneCoins - 1;
            playerTwoCoins = playerTwoCoins + 3;
        }
    }

    public int getPlayerOneCoins() {
        return playerOneCoins;
    }

    public int getPlayerTwoCoins() {
        return playerTwoCoins;
    }
}