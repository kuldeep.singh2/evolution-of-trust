package com.tw.bootcamp;

public class AlwaysCheat extends Strategy {
    @Override
    public Move getNextMove() {
        return Move.CHEAT;
    }
}
