package com.tw.bootcamp;

public abstract class Strategy {

    public abstract Move getNextMove();
}
