package com.tw.bootcamp;

public class EvolutionOfTrustGame {

    private RuleEngine ruleEngine;
    private int numberOfRounds;

    public EvolutionOfTrustGame(RuleEngine ruleEngine, int numberOfRounds) {
        this.ruleEngine = ruleEngine;
        this.numberOfRounds = numberOfRounds;
    }

    public String play(Player p1, Player p2) {
        for (int i = 0; i < numberOfRounds; i++) {
            this.ruleEngine.play(p1.getMove(), p2.getMove());
        }
        return "Player 1 coins is " + ruleEngine.getPlayerOneCoins() + " Player 2 coins is " + ruleEngine.getPlayerTwoCoins();
    }
}
